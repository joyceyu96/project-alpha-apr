# Generated by Django 4.1.1 on 2022-09-14 18:00

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("tasks", "0001_initial"),
    ]

    operations = [
        migrations.AlterField(
            model_name="task",
            name="is_complete",
            field=models.BooleanField(null=True),
        ),
    ]

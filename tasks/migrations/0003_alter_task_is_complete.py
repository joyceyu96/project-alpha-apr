# Generated by Django 4.1.1 on 2022-09-14 21:14

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("tasks", "0002_alter_task_is_complete"),
    ]

    operations = [
        migrations.AlterField(
            model_name="task",
            name="is_complete",
            field=models.BooleanField(default=False, null=True),
        ),
    ]
